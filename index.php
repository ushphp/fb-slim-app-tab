<?php
define('DS', DIRECTORY_SEPARATOR);
define('DIR', __DIR__);

require 'Slim/Slim.php';
require 'config/main-matias.php';
require 'extras/RainView.php';
require 'vendors/rb.php';

RainView::$rainDirectory = 'vendors';
RainView::$rainCacheDirectory = 'cache/';
$app = new Slim(array(
    'view' => 'RainView',
));
R::setup(Config::$DB_CONN, Config::$DB_USER, Config::$DB_PASS);


$app->get('/', function () use ($app) {
    $app->render('index');
});

$route = 'test';
$app->get('/'.$route, function () use ($app, $route) {
    $book = R::dispense( 'book' );
    $book->title = 'Boost development with RedBeanPHP';
    $book->author = 'Charles Xavier'; 
    $id = R::store($book);
    $app->render($route, array('name' => 'Matias'));
});

//POST route
$app->post('/post', function () {
    echo 'This is a POST route';
});

//PUT route
$app->put('/put', function () {
    echo 'This is a PUT route';
});

//DELETE route
$app->delete('/delete', function () {
    echo 'This is a DELETE route';
});

$app->run();
R::close();
